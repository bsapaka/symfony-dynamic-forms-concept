<?php

namespace ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ranged
 *
 * @ORM\Entity(repositoryClass="ItemBundle\Repository\RangedRepository")
 */
class Ranged extends Weapon {

    protected $subEntities = [];

    /**
     * @var float
     *
     * @ORM\Column(name="rng_range", type="float", nullable=true, options={"alias":"range"})
     */
    protected $rngRange;

    /**
     * @var string
     *
     * @ORM\Column(name="rng_projectile", type="string", length=255, nullable=true, options={"alias":"projectile"})
     */
    protected $rngProjectile;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rngRange
     *
     * @param float $rngRange
     * @return Ranged
     */
    public function setRngRange($rngRange)
    {
        $this->rngRange = $rngRange;

        return $this;
    }

    /**
     * Get rngRange
     *
     * @return float 
     */
    public function getRngRange()
    {
        return $this->rngRange;
    }

    /**
     * Set rngProjectile
     *
     * @param string $rngProjectile
     * @return Ranged
     */
    public function setRngProjectile($rngProjectile)
    {
        $this->rngProjectile = $rngProjectile;

        return $this;
    }

    /**
     * Get rngProjectile
     *
     * @return string 
     */
    public function getRngProjectile()
    {
        return $this->rngProjectile;
    }
}
