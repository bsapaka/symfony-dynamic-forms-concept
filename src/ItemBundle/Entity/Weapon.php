<?php

namespace ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Weapon
 *
 * @ORM\Entity(repositoryClass="ItemBundle\Repository\WeaponRepository")
 */
class Weapon extends Item {

    /**
     * @var array
     */
    protected $subEntities = [
        Melee::class,
        Ranged::class
    ];

    /**
     * @var bool
     *
     * @ORM\Column(name="wpn_hands", type="boolean", nullable=true, options={"alias":"handed"})
     */
    protected $wpnHands;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wpnHands
     *
     * @param boolean $wpnHands
     * @return Weapon
     */
    public function setWpnHands($wpnHands)
    {
        $this->wpnHands = $wpnHands;

        return $this;
    }

    /**
     * Get wpnHands
     *
     * @return boolean 
     */
    public function getWpnHands()
    {
        return $this->wpnHands;
    }
}
