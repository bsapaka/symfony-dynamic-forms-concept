<?php

namespace ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * Item
 *
 * @ORM\Table(name="item")
 * @ORM\Entity(repositoryClass="ItemBundle\Repository\ItemRepository")
 * @InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="item_type", type="string")
 * @ORM\DiscriminatorMap({"item" = "Item", "weapon" = "Weapon", "melee" = "Melee", "ranged" = "Ranged"})
 */
class Item {

    /**
     * @var array
     */
    protected $subEntities = [
        Weapon::class
    ];
    
    public function subEntities() {
        return $this->subEntities;
    } 
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="item_name", type="string", length=255, options={"alias":"name"})
     *
     */
    protected $itemName;

    /**
     * @var float
     *
     * @ORM\Column(name="item_weight", type="float", options={"alias":"weight"})
     */
    protected $itemWeight;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemName
     *
     * @param string $itemName
     * @return Item
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * Get itemName
     *
     * @return string 
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * Set itemWeight
     *
     * @param float $itemWeight
     * @return Item
     */
    public function setItemWeight($itemWeight)
    {
        $this->itemWeight = $itemWeight;

        return $this;
    }

    /**
     * Get itemWeight
     *
     * @return float 
     */
    public function getItemWeight()
    {
        return $this->itemWeight;
    }
}
