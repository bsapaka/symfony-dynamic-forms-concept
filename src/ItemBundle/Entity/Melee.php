<?php

namespace ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Melee
 *
 * @ORM\Entity(repositoryClass="ItemBundle\Repository\MeleeRepository")
 */
class Melee extends Weapon {
    
    protected $subEntities = [];

    /**
     * @var float
     *
     * @ORM\Column(name="mel_oal", type="float", nullable=true, options={"alias":"OAL"})
     */
    protected $melOal;

    /**
     * @var float
     *
     * @ORM\Column(name="mel_pob", type="float", nullable=true, options={"alias":"POB"})
     */
    protected $melPob;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set melOal
     *
     * @param float $melOal
     * @return Melee
     */
    public function setMelOal($melOal)
    {
        $this->melOal = $melOal;

        return $this;
    }

    /**
     * Get melOal
     *
     * @return float 
     */
    public function getMelOal()
    {
        return $this->melOal;
    }

    /**
     * Set melPob
     *
     * @param float $melPob
     * @return Melee
     */
    public function setMelPob($melPob)
    {
        $this->melPob = $melPob;

        return $this;
    }

    /**
     * Get melPob
     *
     * @return float 
     */
    public function getMelPob()
    {
        return $this->melPob;
    }
}
