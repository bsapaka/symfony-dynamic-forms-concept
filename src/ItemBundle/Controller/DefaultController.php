<?php

namespace ItemBundle\Controller;

use ItemBundle\Entity\Item;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    /**
     * @Route("/test")
     */
    public function testAction() {

        $em = $this->getDoctrine()->getEntityManager();
        $item = $em->getRepository('ItemBundle:Item')->find(1);

        $meta = $em->getClassMetadata(get_class($item));
        $fieldMeta = [];
        foreach ($meta->getFieldNames() as $fieldName) {
            $fieldMeta[] = $meta->getFieldMapping($fieldName);
        }

        return Response::create(dump($fieldMeta));
    }
}
