<?php namespace ItemBundle\Controller;


use ItemBundle\Entity\Item;

use ItemBundle\Form\ItemType as ItemForm;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//Injections
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Form\FormFactory;

/**
 * Class ItemController
 * @package ItemBundle\Controller
 *
 * @Route("/item", service="item.item_controller")
 */
class ItemController {

	/**
	 * @var FormFactory
	 */
	protected $formFactory;

	/**
	 * @var EntityManager
	 */
	protected $em;

	protected $twig;
	
	public function __construct(
		FormFactory $formFactory, 
		EntityManager $em,
		TwigEngine $twig
	) {
		$this->formFactory = $formFactory;
		$this->em = $em;
		$this->twig = $twig;
	}

	/**
	 * Creates a new Item entity.
	 *
	 * @param Request $request
	 * @param String $type
	 * @return mixed
	 *
	 * @Route("/new/{type}", name="item_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction(Request $request, $type)
	{
		$class = $this->em->getClassMetadata('ItemBundle\Entity\Item')->discriminatorMap[$type];

		$item = new $class;
		$form = $this->formFactory->create(new ItemForm($this->em, $item), $item);
		$form->handleRequest($request);

		$content = $this->twig->render('ItemBundle:item:new.html.twig', array(
			'item' => $item,
			'form' => $form->createView(),
		));

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->em;
			$em->persist($item);
			$em->flush();

//			return $this->redirectToRoute('item_show', array('id' => $item->getId()));
		}

		return Response::create($content);
	}

	/**
	 * @Route("/", name="item_index")
	 */
	public function indexAction() {
		return Response::create('index');
	}

	/**
	 * @param Item $item
	 * @return Response
	 *
	 * @Route("/{item}", name="item_show")
	 */
	public function showAction(Item $item) {
		return Response::create(dump($item));
	}

}

