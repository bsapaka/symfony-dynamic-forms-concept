<?php

namespace ItemBundle\Form;

use Doctrine\ORM\EntityManager;
use ItemBundle\Entity\Item;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{

    /**
     * @var EntityManager;
     */
    protected $em;

    /**
     * @var Item
     * 
     * The leaf class of the item taxonomy
     */
    protected $subItem;

    /**
     * ItemType constructor.
     * @param EntityManager $em
     * @param Item $subItem
     */
    public function __construct( EntityManager $em, Item $subItem) {
        $this->em = $em;
        $this->subItem = $subItem;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //TODO: form item_type here
//        $itemTypeClass = $this->em->getClassMetadata('ItemBundle\Entity\Item')->discriminatorMap[$this->type];

        $itemTypeMeta = $this->em->getClassMetadata(get_class($this->subItem));

        $fieldMeta = [];
        foreach ($itemTypeMeta->getFieldNames() as $fieldName) {
            $fieldMeta[] = $itemTypeMeta->getFieldMapping($fieldName);
        }
        
        foreach ( $fieldMeta as $field ) {
            $options = [];
            if(array_key_exists('options', $field)) {
                if(array_key_exists('alias', $field['options'])) {
                    $alias = ucwords($field['options']['alias']);
                    $options = ['label' => $alias];
                }
            }

            $builder->add($field['fieldName'],null, $options);
        }

        $builder->remove('id');

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ItemBundle\Entity\Item'
        ));
    }
    
}
